#!/usr/bin/env python
# coding: utf-8

import pandas as pd
import psycopg2
import json
import datetime
from influxdb import InfluxDBClient, DataFrameClient
import time
import credentials as cr


# Establish Database Connection
try:
    connection = psycopg2.connect(user=cr.postgres_user, password=cr.postgres_pw, host=cr.postgres_host, database='mydatabase', port=5432)
    print("DB connection established")
except Exception as e:
    print("I am unable to connect to the database:", e)

# Read data from Database
sql_command = 'SELECT * FROM coronatweets'
df = pd.read_sql(sql_command, connection)
df['words'] = df.text.str.strip().str.split('[\W_]+')

rows = list()
for row in df[['user_name','words']].iterrows():
    r = row[1]
    for word in r.words:
        rows.append((r.user_name, word))

words = pd.DataFrame(rows, columns=['user_name', 'word_count'])

words = words[words.word_count.str.len() > 0]
words['word_count'] = words.word_count.str.lower()

counts = pd.DataFrame(words['word_count'])
counts2 = counts.word_count.value_counts().to_frame().sort_values(by=['word_count'], ascending=False)

do_not_show = ['por', 've', 'just', 'my', 'se', 'y', 'if', 'do', 'they', 'from', 'their', 'has', 'me', 'pra', 'with', 'be', 'why', 'are', 'la', 'that', 'el', 'en', 'it', 'on', 'no', 'rt', 'the', 'is', 'a', 'https', 'http', 'for', 't', 'to', 'co', 'in', 'der', 'and', 'de', 'of', 's', 'e', 'this', 'o', 'i', 'que', 'have']
data = counts2.drop(do_not_show, axis=0)

current_milli_time = lambda: int(round(time.time() * 1000))
curr_time = current_milli_time()

client = InfluxDBClient(host=cr.influx_host, port=8086, username=cr.influx_user, password=cr.influx_pw, ssl=False, verify_ssl=False)
zaehler = 1
for row in data.iterrows():
    j = {}
    j['measurement'] = 'coronatweets_top'
    j['tags'] = {'word': row[0]}
    j['time'] = curr_time
    j['fields'] = {'value': int(row[1][0])}
    client.write_points([j], time_precision='ms', database='datahub', protocol=u'json')
    zaehler += 1
    if zaehler == 30:
        break

# delete data in table
dele_command = 'DELETE FROM coronatweets'
cur = connection.cursor()
cur.execute(dele_command)
connection.commit()
connection.close()