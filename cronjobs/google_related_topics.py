import pandas as pd
from pytrends.request import TrendReq
from influxdb import InfluxDBClient
import datetime
import credentials as cr

pytrend = TrendReq(hl='de_DE')
kw_list =['Corona']
pytrend.build_payload(kw_list)

jetzt = datetime.datetime.now()

client = InfluxDBClient(host=cr.influx_host, port=8086, username=cr.influx_user, password=cr.influx_pw, ssl=False, verify_ssl=False)

import time
current_milli_time = lambda: int(round(time.time() * 1000))
curr_time = current_milli_time()

data = pytrend.related_topics()
df = pd.DataFrame(data['Corona']['rising'])
for row in df.iterrows():
    if row[1][4] != 'Cubit':
        j = {}
        j['measurement'] = 'googletopics'
        j['tags'] = {'word': row[1][4]}
        j['time'] = curr_time
        j['fields'] = {'value': int(row[1][0])}
        client.write_points([j], time_precision='ms', database='test', protocol=u'json')
