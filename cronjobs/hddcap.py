import shutil
from influxdb import InfluxDBClient
import credentials as cr
import time

client = InfluxDBClient(host=cr.influx_host, port=8086, username=cr.influx_user, password=cr.influx_pw, ssl=False, verify_ssl=False)

current_milli_time = lambda: int(round(time.time() * 1000))
curr_time = current_milli_time()

total, used, free = shutil.disk_usage("/")

j = {}
j['measurement'] = 'hddcap'
j['tags'] = {'usage': 'free'}
j['time'] = curr_time
j['fields'] = {'value': int(free)}
client.write_points([j], time_precision='ms', database='datahub', protocol=u'json')
