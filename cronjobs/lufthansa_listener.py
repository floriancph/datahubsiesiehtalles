import requests, json, time
import datetime as dt
from datetime import date, timedelta
from influxdb import InfluxDBClient, DataFrameClient
import credentials as cr

### FUNCTIONS ###

def str_to_json(text):
    return json.loads(text.text)

def getAccessToken():
    url = 'https://api.lufthansa.com/v1/oauth/token'
    myobj = {'client_id': cr.lufthansa_client_id, 'client_secret': cr.lufthansa_client_secret, 'grant_type': 'client_credentials'}
    req = str_to_json(requests.post(url, data = myobj))
    return req['access_token']

def api_call_route(date, dep_airport, arrival_airport, header):
    return str_to_json(requests.get(f"https://api.lufthansa.com/v1/operations/flightstatus/route/{dep_airport}/{arrival_airport}/{date}?serviceType=passenger", headers=header))

### MAIN ######

flight_connections = [('FRA', 'TXL')]
today = date.today()
access_token = getAccessToken()
header = {}
header['Authorization'] = 'Bearer ' + access_token
header['Accept'] = 'application/json'

current_milli_time = lambda: int(round(time.time() * 1000))
curr_time = current_milli_time()

for route in flight_connections:
    try:
        result = api_call_route(today, route[0], route[1], header)
        flights = result['FlightStatusResource']['Flights']['Flight']
        no_of_flights = len(flights)

        cancelled = 0
        for flight in flights:
            if flight['FlightStatus']['Definition'] == 'Flight Cancelled':
                cancelled += 1

        client = InfluxDBClient(host=cr.influx_host, port=8086, username=cr.influx_user, password=cr.influx_pw, ssl=False, verify_ssl=False)
        print("DB connection established")
        j = {}
        j['measurement'] = 'lufthansa_flights'
        j['tags'] = {'route': route[0]+'-'+route[1]}
        j['time'] = curr_time
        j['fields'] = {'Anzahl': no_of_flights, 'Cancelled': cancelled}
        client.write_points([j], time_precision='ms', database='datahub', protocol=u'json')

    except:
        print('Failed')
print(f'Finished')