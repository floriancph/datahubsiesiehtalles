import requests, json, time
import datetime as dt
from datetime import date, timedelta
from influxdb import InfluxDBClient, DataFrameClient
import xml.etree.ElementTree as ET
import credentials as cr
from influxdb import InfluxDBClient

### API Configuration ###

def api_call_route(station_id):
    return requests.get(f"http://api.deutschebahn.com/timetables/v1/fchg/{station_id}", headers=header)

station_id = 8002549
access_token = cr.db_api_token
header = {}
header['Authorization'] = 'Bearer ' + access_token

#Datenbank Anbindung
client = InfluxDBClient(host=cr.influx_host, port=8086, username=cr.influx_user, password=cr.influx_pw, ssl=False, verify_ssl=False)

#API Call
result = api_call_route(station_id)

root = ET.fromstring(result.text)

total_delays = 0

#find arrivals
for char in root.findall("*/ar"):
    
    changed_time = char.get("ct")
    planned_time = char.get("pt")
    if changed_time != None and planned_time != None:
        delay = int(changed_time) - int(planned_time)
        total_delays += delay

import time
current_milli_time = lambda: int(round(time.time() * 1000))
curr_time = current_milli_time()

j = {}
j['measurement'] = 'db_delays'
j['tags'] = {'station': station_id}
j['time'] = curr_time
j['fields'] = {'value': total_delays}
client.write_points([j], time_precision='ms', database='test', protocol=u'json')
