import requests, json, time, datetime
from influxdb import InfluxDBClient, DataFrameClient
import credentials as cr

url = "https://api.public.fusionbase.io/cases"

payload = {}
headers = {
  'X-API-Key': cr.fusionbase_key
}

start = time.time()
response = requests.request("GET", url, headers=headers, data = payload)
response = json.loads(response.text.encode('utf8'))
end = time.time()
print(f'API call took {end - start} seconds')

liste_all = sorted(response, key = lambda i: i['cases'],reverse=True)

client = InfluxDBClient(host=cr.influx_host, port=8086, username=cr.influx_user, password=cr.influx_pw, ssl=False, verify_ssl=False)
print("DB connection established")

current_milli_time = lambda: int(round(time.time() * 1000))
curr_time = current_milli_time()

start = time.time()
liste_all_for_db = []
for lk in liste_all:
  try:
    publication_datetime = (datetime.datetime.strptime(lk['publication_datetime'], '%Y-%m-%dT%H:%M:%S')).date()
  except:
    publication_datetime = 0
    print(f'datetime not available for element {lk["kreis_name"]}')

  if publication_datetime == datetime.datetime.now().date():
    j = {}
    j['measurement'] = 'corona_faelle_landkreise'
    j['tags'] = {'landkreis_name': lk['kreis_name'], 'bundesland_name': lk['bundesland_name'], 'publication_date': publication_datetime}
    j['time'] = curr_time
    j['fields'] = {'faelle': lk['cases']}
    liste_all_for_db.append(j)

liste_top_5 = liste_all_for_db[:5]
for i in range(len(liste_top_5)):
  liste_top_5[i]['measurement'] = 'corona_faelle_landkreise_top_5'

client.write_points(liste_all_for_db, time_precision='ms', database='datahub', protocol=u'json')
client.write_points(liste_top_5, time_precision='ms', database='datahub', protocol=u'json')

end = time.time()
print(f'Writing to db took {end - start} seconds')