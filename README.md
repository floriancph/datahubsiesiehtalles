# WIRVSVIRUSHACKATHON - DATA HUB FÜR DEUTSCHLAND

## Data Hub 

*  Dashboard: [Link](http://grafana.siesiehtalles.de:3000/d/s837cm9Wz/corona-dashboard?orgId=1&refresh=1m) (**Sign-In**: Username: viewer, password: viewer)

*  jupyter-Notebooks: [Link](https://ec2-3-121-220-123.eu-central-1.compute.amazonaws.com:8888/)

*  Data Catalogue with API Specifications: [Link](https://siesiehtalles.github.io/jkan)