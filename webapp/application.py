from flask import Flask, render_template, Response
import db_wrapper, requests, json
app = Flask(__name__)

@app.route('/', methods=['GET'])
def index():
    top_three_news = db_wrapper.getTopThreeNews() # Get Top Three Trends
    total_news_results = db_wrapper.getTotalNewsResults() # Get Total Number of News Articles (in order to calculate percentage)
    return render_template('index.html', top_three_news=top_three_news, total_news_results=total_news_results)

@app.route('/social', methods=['GET'])
def social():
    db_result = db_wrapper.getSocial() # Get latest Trends (db-write within last 60mins. & sorted by no. of articles)
    top_news = db_wrapper.getTopNews() # Get the Top-News of today from the news.api
    x_values = []
    y_values = []

    if len(db_result) > 0:
        for trend in db_result:
            x_values.append(trend['number_of_articles'])
            y_values.append(trend['trend'])

    if len(top_news) > 0:
        top_news = top_news[0]

    return render_template('social.html', db_result=db_result, top_news=top_news, x_values=x_values, y_values=y_values)

@app.route('/hashtag/<hashtag>', methods=['GET'])
def byHashtag(hashtag):
    hashtag = hashtag.lower()
    db_result = db_wrapper.getTrendsByTwitterHashtag(hashtag)
    list_of_trends = db_result[0]
    datum = json.dumps(db_result[1])
    werte = db_result[2]
    return render_template('hashtag.html', list_of_trends=list_of_trends, hashtag=hashtag, datum=datum, werte=werte)

@app.route('/graph', methods=['GET'])
def graph():
    result = db_wrapper.KnowledgeGraph()
    if result != 0:
        return render_template('force.html', result=result)
    else:
        return render_template('error.html')

@app.route('/getMyJson')
def getMyJson():
    result = db_wrapper.KnowledgeGraph()
    response = Response(response=json.dumps(result), status=200, mimetype="application/json")
    return(response)


######## ERROR HANDLING ########

@app.errorhandler(404)
def page_not_found(e):
    return render_template('error.html'), 404

@app.errorhandler(505)
def internal_server_error(e):
    return render_template('error.html'), 505

# start server:
# navigate into folder
# export FLASK_APP=application.py
# export FLASK_ENV=development
# flask run