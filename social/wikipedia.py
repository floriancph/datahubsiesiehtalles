import wikipediaapi
import pandas as pd

def getWikiInfo(df):
    """Receives dataframe"""
    wiki_wiki = wikipediaapi.Wikipedia('de')
    wiki_summary = []
    wiki_url = []
    for element in df['title']:
        page_py = wiki_wiki.page(element)
        wiki_summary.append(page_py.summary)
        if page_py.summary != '':
            wiki_url.append(page_py.fullurl)
        else:
            wiki_url.append('')
    df['wiki_summary'] = wiki_summary
    df['wiki_url'] = wiki_url
    return df