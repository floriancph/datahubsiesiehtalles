import credentials as cr 
import requests
import json

def addNumberOfArticles(df):
    all_number_of_articles = []
    for element in df['title']:
        element = element.replace(" ", "%20")
        element = element.replace("(", "")
        r = requests.get(f'http://newsapi.org/v2/everything?q={element}&sortBy=publishedAt&apiKey={cr.news_api_token}')
        r = json.loads(r.text)
        if len(r) > 0:
            number_of_articles = r['totalResults']
        else:
            number_of_articles = 0
        all_number_of_articles.append(number_of_articles)
    df['number_of_articles'] = all_number_of_articles
    return df

def getTopNews(country):
    r = requests.get(f'https://newsapi.org/v2/top-headlines?country={country}&category=general&apiKey={cr.news_api_token}')
    r = json.loads(r.text)
    return r['articles']