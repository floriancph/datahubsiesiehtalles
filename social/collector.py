import pandas as pd
import google_trends, twitter, wikipedia, news
import time
from influxdb import InfluxDBClient
import credentials as cr

start = time.time()
country = 'germany'
lang = 'de'
database = 'test'

df = google_trends.getGoogleTrends(country) # check google trends - receives a dataframe
df.rename(columns={0:'title'}, inplace=True)
print('Google Trends done...')

df = wikipedia.getWikiInfo(df) # extend the df with wiki entries
print('Wikipedia done...')

df = twitter.collector(df, lang) # extend the df with twitter information
print('Twitter done...')

df = news.addNumberOfArticles(df) # add number of articles
news_list = news.getTopNews(lang)
print('News done...')

### WRITE TO DATABASE ###

client = InfluxDBClient(host=cr.influx_host, port=8086, username=cr.influx_user, password=cr.influx_pw, ssl=False, verify_ssl=False)
current_milli_time = lambda: int(round(time.time() * 1000))
curr_time = current_milli_time()

for i in range(0, len(df)):
    j = {}
    j['measurement'] = 'social'
    j['tags'] = {'country': country, 'lang': lang, 'trend': f"{df.iloc[i].title}"}
    j['time'] = curr_time
    j['fields'] = {
        'wiki_summary': f"{df.iloc[i].wiki_summary}", 
        'wiki_url': f"{df.iloc[i].wiki_url}",
        'twitter_hashtags': f"{df.iloc[i].twitter_hashtags}",
        'influencial_tweets': f"{df.iloc[i].influencial_tweets}",
        'number_of_articles': df.iloc[i].number_of_articles
        }
    client.write_points([j], time_precision='ms', database=database, protocol=u'json')

for i in news_list:
    j = {}
    j['measurement'] = 'top_news'
    j['tags'] = {'news_id': i['source']['id'], 'news_name': i['source']['name']}
    j['time'] = curr_time
    j['fields'] = {
        'author': i['author'], 
        'title': i['title'],
        'description': i['description'],
        'publishedAt': i['publishedAt'],
        'url': i['url'],
        'urlToImage': i['urlToImage'],
        'content': i['content']
        }
    client.write_points([j], time_precision='ms', database=database, protocol=u'json')

print('Database write operation done')

stop = time.time()
print('Time needed:', stop - start)