from pytrends.request import TrendReq
import pandas as pd

def getGoogleTrends(country):
    pytrend = TrendReq()
    return pytrend.trending_searches(pn=country)