import pandas as pd
import TOKEN
import tweepy
from tweepy.parsers import JSONParser

def getTweets(query, lang, count):
    auth = tweepy.OAuthHandler(TOKEN.API_KEY, TOKEN.API_SECRET_KEY)
    auth.set_access_token(TOKEN.ACCESS_TOKEN, TOKEN.ACCESS_TOKEN_SECRET)
    api = tweepy.API(auth, parser=tweepy.parsers.JSONParser())
    return api.search(query, lang=lang, count=count)

def getInfluencialTweets(tweets):
    """ Calculates the three most influencial tweets - measured by count of retweets """
    influencial_tweets = []
    for tweet in tweets:
        if 'retweeted_status' not in tweet:
            tweet_url = 'https://twitter.com/i/status/' + tweet['id_str']
            retweet_count = tweet['retweet_count']
            influencial_tweets.append([retweet_count, tweet_url])
    
    # sort list
    def takeFirst(elem):
        return elem[0]
    
    influencial_tweets.sort(key=takeFirst, reverse=True)
    influencial_return = []
    for tweet in influencial_tweets:
        influencial_return.append(tweet[1])
    return influencial_return[:3]


def collector(df, lang):
    
    all_hashtags = []
    all_influencial_tweets = []
    for trend in df['title']:
        # COLLECT HASHTAGS
        tweets = getTweets(trend, lang, 50)
        tweets = tweets['statuses']
        hashtags = {}
        for tweet in tweets:
            if len(tweet['entities']['hashtags']) > 0:
                for hashtag in tweet['entities']['hashtags']:
                    if hashtag['text'].lower() in hashtags:
                        hashtags[hashtag['text'].lower()] += 1
                    else:
                        hashtags[hashtag['text'].lower()] = 1
        hashtags = list(hashtags.keys())
        all_hashtags.append(hashtags)

        # COLLECT INFLUENCIAL TWEETS
        influencial_tweets = getInfluencialTweets(tweets)
        all_influencial_tweets.append(influencial_tweets)
    
    df['twitter_hashtags'] = all_hashtags
    df['influencial_tweets'] = all_influencial_tweets
    return df
