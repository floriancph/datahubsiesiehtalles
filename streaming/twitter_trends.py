import TOKEN
import tweepy
from influxdb import InfluxDBClient
import time
import credentials as cr

auth = tweepy.OAuthHandler(TOKEN.API_KEY, TOKEN.API_SECRET_KEY)
auth.set_access_token(TOKEN.ACCESS_TOKEN, TOKEN.ACCESS_TOKEN_SECRET)
api = tweepy.API(auth)

# trends_available = api.trends_available()
trends_place = api.trends_place(id=638242)

client = InfluxDBClient(host=cr.influx_host, port=8086, username=cr.influx_user, password=cr.influx_pw, ssl=False, verify_ssl=False)
current_milli_time = lambda: int(round(time.time() * 1000))
curr_time = current_milli_time()

for i in trends_place[0]['trends']:

    if i['tweet_volume'] != None:
        j = {}
        j['measurement'] = 'twitter_trends'
        j['tags'] = {'city': trends_place[0]['locations'][0]['name'], 'trend': i['name']}
        j['time'] = curr_time
        j['fields'] = {'tweet_volume': i['tweet_volume']}
        client.write_points([j], time_precision='ms', database='test', protocol=u'json')
        print(j)
    else:
        pass

print('successfull')
print("jo")