import TOKEN
import tweepy
from textblob import TextBlob
import psycopg2
import json
import requests
import datetime
import credentials as cr

try:
    connection = psycopg2.connect(user=cr.postgres_user, password=cr.postgres_pw, host=cr.postgres_host, database='mydatabase', port=5432)
    print("DB connection established")
except Exception as e:
    print("I am unable to connect to the database:", e)


class StreamListener(tweepy.StreamListener):

    def on_status(self, status):
        if status.retweeted:
            return
        
        description = str(status.user.description).strip().replace("'"," ")
        loc = str(status.user.location).strip().replace("'"," ")
        text = str(status.text).strip().replace("'"," ")
        coords = str(status.coordinates).strip().replace("'"," ")
        geo = str(status.geo).strip().replace("'","")
        name = str(status.user.screen_name).strip().replace("'","")
        user_created = str(status.user.created_at).strip().replace("'","")
        followers = status.user.followers_count
        id_str = str(status.id_str).strip().replace("'","")
        created = str(status.created_at).strip().replace("'","")
        retweets = status.retweet_count
        bg_color = str(status.user.profile_background_color).strip().replace("'"," ")
        blob = TextBlob(text)
        sent = blob.sentiment

        if geo is not None:
            geo = json.dumps(geo)

        if coords is not None:
            coords = json.dumps(coords)
   
        cursor = connection.cursor()

        #insert_query = f"INSERT INTO tweets (text, user_followers) VALUES ('{str(text)}', {followers})"
        try:
            insert_query = f"""INSERT INTO coronatweets (
                user_location,
                text,
                user_name,
                user_created,
                user_followers,
                id_str,
                created,
                retweet_count,
                user_bg_color,
                polarity,
                subjectivity) VALUES (
                    '{loc}',
                    '{text}',
                    '{name}',
                    '{user_created}',
                    {followers},
                    '{id_str}',
                    '{created}',
                    {retweets},
                    '{bg_color}',
                    '{sent.polarity}',
                    '{sent.subjectivity}')"""

            cursor.execute(insert_query)
            connection.commit()
            print("success", datetime.datetime.now())
        except (Exception, psycopg2.Error) as error :
            if(connection):
                cur = connection.cursor()
                cur.execute('ROLLBACK;')
                connection.commit()
                print("Failed to insert record into tweet table", error)
                resp = requests.get('https://flwxee4g5h.execute-api.eu-central-1.amazonaws.com/test/transactions?transactionId=2&type=PURCHASE&amount=3')
                return True

    def on_error(self, status_code):
        if status_code == 420:
            resp = requests.get('https://flwxee4g5h.execute-api.eu-central-1.amazonaws.com/test/transactions?transactionId=2&type=PURCHASE&amount=3')
            print("Fail")
            #returning False in on_data disconnects the stream
            return False
        else:
            resp = requests.get('https://flwxee4g5h.execute-api.eu-central-1.amazonaws.com/test/transactions?transactionId=2&type=PURCHASE&amount=3')
            print("Fail")

def start_stream(stream):
    try:
        stream.filter(track=['Corona', 'COVID-19'])
        print("filtering")
    except:
        print("Restart Stream")
        stream.disconnect()
        start_stream(stream)

auth = tweepy.OAuthHandler(TOKEN.API_KEY, TOKEN.API_SECRET_KEY)
auth.set_access_token(TOKEN.ACCESS_TOKEN, TOKEN.ACCESS_TOKEN_SECRET)
api = tweepy.API(auth)

stream_listener = StreamListener()
print("Listener established")
mystream = tweepy.Stream(auth=api.auth, listener=stream_listener)
start_stream(mystream)